# meetdown.io
## Réaliser la plateforme Meetdown.io, qui permet aux gens d'organiser des évènements publiques.

- La page d'acceuil listera tous les évènements avec une pagination.
- Les gens pourront également s'inscrire, se connecter et poster des évènements quand ils sont connectés. 
- Le formulaire permettant de poster un évènement comportera au moins 5 types de champs différents.

## Images

- ![](snapshots/1.png)
- ![](snapshots/2.png)
- ![](snapshots/3.png)
- ![](snapshots/4.png)
- ![](snapshots/5.png)
- ![](snapshots/6.png)

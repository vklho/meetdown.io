<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Event;
use App\Repository\EventRepository;
use App\Form\EventType;

class EventController extends AbstractController
{
    /**
     * @Route("/events", name="events")
     */
    public function events(EventRepository $repository)
    {
      $events = $repository->findAll();
      return $this->render('events/all.html.twig', [
        'events' => $events
      ]);
    }

    /**
     * @Route("/events/{id}", name="event")
     */
    public function event(Event $event)
    {
      return $this->render('events/byId.html.twig', [
        'event' => $event
      ]);
    }

    /**
     * @Route("/publish", name="publish")
     * @Route("/events/{id}/edit", name="event_edit")
     * @Security("has_role('ROLE_USER')")
     */
    public function publish(Event $event = null, Request $request)
    {
      $event = $event ? $event : new Event();
      $form = $this->createForm(EventType::class, $event);
      $form->handleRequest($request);

      return $form->isSubmitted() && $form->isValid()
        ? $this->_put($event)
        : $this->render('events/form.html.twig', [
        'form' => $form->createView(),
        'isEditMode' => $event->getId()
      ]);
    }

    private function _put(Event $event)
    {
      $manager = $this->getDoctrine()->getManager();
      if (!$event->getId()) {
        $event->setCreatedAt(new \DateTime());
      }
      $manager->persist($event);
      $manager->flush();
      return $this->redirectToRoute('event', [
        'id' => $event->getId()
      ]);
    }
  }

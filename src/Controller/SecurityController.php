<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\User;
use App\Form\SignInType;
use App\Form\LoginType;

class SecurityController extends AbstractController
{
    /**
     * @Route("/signin", name="signin")
     */
    public function signin(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
      $user = new User();
      $form = $this->createForm(SignInType::class, $user);

      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $hash = $encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($hash);
        $manager->persist($user);
        $manager->flush();

        return $this->redirectToRoute('login');
      }

      return $this->render('security/signin.html.twig', [
        'form' => $form->createView()
      ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
      $error = $authenticationUtils->getLastAuthenticationError();
      $form = $this->createForm(LoginType::class);

      return $this->render('security/login.html.twig', [
        'error' => $error,
        'form' => $form->createView()
      ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout() {}
  }
